<?php
/***
 * 
 * Will display our data with the following requirements
 * Per each day:
 * 1. Total and number of sales per sales agent
 * 2. Total and number of sales per product
 * 3. Total and number of sales per customer
 * 
 */

/**
 * 
 * Due to lack of time to learn Laravel and following on our first interview (mentioning that framework could be changed), I have opted for a direct approach for doing the task.
 * I will be using a combination of array_reduce and standard loops for achieving this task
 * 
 */


$db_servername = "";
$db_username = "";
$db_password = "";
$db_name = "";

$connection = new PDO("mysql:host=$db_servername;dbname=$db_name", $db_username, $db_password);

/**
 * Will fetch data (if SELECT), from db
 * @param string $sql Query to send to Sql db
 * @param PDO $connection PDO connection
 * @return array Associated array of all data for request ([id => data])
 */
function get_data(string $sql, PDO $connection): array {
    $statement = $connection->prepare($sql);
    $connection->beginTransaction();
    $statement->execute();
    $connection->commit();
    // We return an associated array with id => value, it will be easier to get data further down
    return array_reduce($statement->fetchAll(PDO::FETCH_ASSOC), function($c, $i) {
        $c[$i["id"]] = $i;
        return $c;
    }, []);
}

// Collecting all data from DB, converting array with id => data
$global_data = [];
$global_data["customers"] = get_data("SELECT * FROM `Customers`", $connection);
$global_data["products"] = get_data("SELECT * FROM `Products`", $connection);
$global_data["sales_agents"] = get_data("SELECT * FROM `SalesAgents`", $connection);
$sales = get_data("SELECT * FROM `Sales`", $connection);

// Laravel uses a Cache object to save data Cache::remember..., there would be other way to cache it, using a session variable, would require secure connection first.

// Reducing sales to combine data in a unique associated array
$data_per_day = array_reduce($sales, function ($c, $sale) {
    // sales day
    $day = date("Y-m-d", $sale["datetime"]);
    
    // Initialising $c[$day] if it doesn't exist
    if (!isset($c[$day])) {
        $c[$day] = [
            "sales_agent" => [], // List of sales agents
            "product" => [], // List of products
            "customer" => [], // List of customers
        ];
    }
    // For each of those items, incrementing the number of sales
    foreach (["sales_agent", "product", "customer"] as $i) {
        // Initialising the data if first sale of the day
        if (!isset($c[$day][$i][$sale[$i]])) {
            $c[$day][$i][$sale[$i]] = [
                "total" => $sale["price"], // Total of sales
                "sales" => 1 // Number of sales
            ];
        }
        // Adding price to total and incrementing the number of sales
        else {
            $c[$day][$i][$sale[$i]]["total"] += $sale["price"];
            $c[$day][$i][$sale[$i]]["sales"] ++;
        }
    }
    
}, []);


?>

<!DOCTYPE Html>
<html lang="en">
    <head>
        <title>Number of sales and total price per sales agent / product / customer for each day</title>
    </head>
    <body>
    <?php foreach ($data_per_day as $day => $day_data) { ?>
        <div>Day: <?php echo $day ?></div>
        <!--Table to display total and sales per type -->
        <?php foreach (["sales_agent" => "Sales Agent", "product" => "Product", "customer" => "Customer"] as $type => $label) {?>
        <table>
            <thead>
                <tr>
                    <td><?php echo $label; ?></td>
                    <td>Total price</td>
                    <td>Number of sales</td>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($day_data[$type] as $id => $sales) { ?>
                <tr>
                    <td><?php echo $global_data[$id]["name"]; ?></td>
                    <td><?php echo $sales["total"]; ?></td>
                    <td><?php echo $sales["sales"]; ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php } ?>
    <?php } ?>
    </body>
</html>

